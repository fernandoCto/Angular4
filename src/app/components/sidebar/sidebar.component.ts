import { Component, OnInit } from '@angular/core';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
//https://material.io/icons/ => obtener iconos " " reemplaze "_" para iconos de dos nombres

export const ROUTES: RouteInfo[] = [
    { path: 'dashboard', title: 'Panel de Administrativo',  icon: 'dashboard', class: '' },
    { path: 'user-profile', title: 'Perfil de usuario',  icon: 'person_add', class: '' },
    { path: 'table-list', title: 'Lista de Tabla',  icon: 'content_paste', class: '' },
    { path: 'typography', title: 'Tipografía',  icon: 'library_books', class: '' },
    { path: 'icons', title: 'Iconos',  icon: 'bubble_chart', class: '' },
    { path: 'notifications', title: 'Notificaciones',  icon: 'notifications', class: '' },
    { path: 'upgrade', title: 'Actualizar PRO',  icon: 'unarchive', class: 'active-pro' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  usuario: string;
  nombreApp: string;
  constructor() {
      this.usuario = 'Nombre de Usuario';
      this.nombreApp = 'Sistema';
   }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
}
